module.exports = function (grunt) {

  // Auto load all grunt tasks instead of having to update the file to add one.
  require('load-grunt-tasks')(grunt);

  grunt.initConfig({
    phantomcss: {
      options: {
        viewportSize: [1280, 800],
        mismatchTolerance: 0.75,
        rootUrl: 'http://localhost/'
      },
      src: [
        'test/*.js'
      ]
    }
  });

  // Custom Grunt tasks here, automate your life.
  grunt.registerTask('default', ['phantomcss']);
};
